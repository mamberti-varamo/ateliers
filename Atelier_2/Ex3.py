from math import *

def discriminant(a : float, b : float, c : float)->float:
    return pow(b, 2) - 4 * a * c

def racine_unique(a : float, b : float)->float:
    if a != 0:
        return -b / 2 * a
    else:
        return 0

def racine_double(a : float, b : float, delta : float, num : int)->float:
    if (num == 1):
        return (-b + sqrt(delta)) / 2 * a
    else:
        return (-b - sqrt(delta)) / 2 * a

def str_equation(a : float, b : float, c : float)->str:
    return a + "x2 + " + b + "x + " + c

def solution_equation(a : float, b : float, c : float)->str:
    delta = discriminant(a, b, c)
    string = str_equation(a, b, c)

    if (not delta):
        return ("Solution de l'équation " + string + ": Racine unique " + racine_unique(a, b, c)) 
    elif (delta < 0):
        return ("Solution de l'équation " + string + " Pas de racine réelle")
    else:
        return ("Solution de l'équation " + string + ": Deux racines, x1 = " + racine_double(a, b, c, delta, 1) + " et x2 = " + racine_double(a, b, c, delta, 1))
    