from Ex2 import *

def date_est_valide(jour : int, mois : int, annee : int)->bool:
    if (mois < 1 or mois > 12):
        return False
    elif (mois == 2 and est_bissextile(annee) and (jour < 1 or jour > 29):
        return False
    elif (mois == 2 and not est_bissextile(annee) and (jour < 1 or jour > 28)):
        return False 
    elif (mois != 7 and not mois % 2 and (jour < 1 or jour > 30)):
        return False
    elif (jour < 1 or jour > 31):
        return False
    else:
        return True