# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 14:42:38 2020

@author: user
"""


import __future__
import matplotlib.pyplot as plt

__file__       = "Untitled-1.py"
__author__     = "John Doe"
__copyright__  = "Copyright 2007, The Cogent Project"
__credits__    = ["John Doe"]
__license__    = "Open Source"
__version__    = "1.0"
__maintainer__ = "John Doe"
__email__      = "john.doe@example.com"
__status__     = "Development"

def somme(L):
    """Make the sum of the elements of a list
    Keyword argument:
    L -- List of numbers
    return the sum"
    """
    s = 0
    for i in range(len(L)):
        s = s + L[i]
    return s

def somme2(L):
    """Make the sum of the elements of a list
    Keyword argument:
    L -- List of numbers
    return the sum"
    """
    s = 0
    for e in L:
        s += e
    return s

def somme3(L):
    """Make the sum of the elements of a list
    Keyword argument:
    L -- List of numbers
    return the sum"
    """
    s = 0
    i = 0
    while i <len(L):
        s += L[i]
        i+=1
    return s

l = [1,2,3,4,5,6,7,8,9]
print(somme(l))
print(somme2(l))
print(somme3(l))

def test_exercice1():
    print("TEST SOMME")
    #test liste vide
    print("Test liste vide :", somme([]))
    
    S = [1,10,100,1000,10000]
    print("Test somme 11111 :", somme(S))

test_exercice1()

def moyenne(L):
    """Make the average of the elements of a list
    Keyword argument:
    L -- List of numbers
    return the average"
    """
    if len(L)==0:
        return 0
    s = somme(L)
    m = s/len(L)
    return m
        
def nb_sup(L,e):
    """Calculate the number of elements of a list that are > e
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the number of elements of a list that are > e"
    """
    s = 0
    for i in range(len(L)):
        if e<L[i]:
            s+=1
    return s

def nb_sup1L(L,e):
    """Calculate the number of elements of a list that are > e
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the number of elements of a list that are > e"
    """
    s = 0
    for i in L:
        if e<i:
            s+=1
    return s

def moy_sup(L,e):
    """Calculate the average of elements of a list that are > e
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the number of elements of a list that are > e"
    """
    s = 0
    for i in L:
        if e<i:
            s+=i
    s = s/nb_sup(L,e)
    return s

def val_max(L):
    """Find the bigger value of a list
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the number of elements of L"
    """
    m = 0
    for e in L:
        if e>=m:
            m = e
    return m

def ind_max(L):
    """Find the index bigger value of a list
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the index of the bigger element of L"
    """
    m = 0
    ind = 0
    for i in (len(L)):
        if L[i]>=m:
            m = L[i]
            ind = i
    return m

def position(L,e):
    """Find the position of an entry in a list
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the index of the entry"
    """
    ind = -1
    for i in range(len(L)):
        if L[i] == e:
            ind = i
    return ind

def position2(L,e):
    """Find the position of a entry in a list
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the index of the searched element"
    """
    i = 0
    ind = -1
    while i < len(L):
        if L[i] == e:
            ind = i
            i = len(L)
        i+=1
    return ind

def nb_occurences(L,e):
    """Count the number of occurences
    Keyword argument:
    L -- List of numbers
    e -- a number
    return the number of occurence"
    """
    s = 0
    for i in L:
        if i == e:
            s += 1
    return s

def est_triee(L):
    """Determine if a list is sorted
    Keyword argument:
    L -- List of numbers
    return true or false"
    """
    last = L[0]
    for i in range(1,len(L)):
        if L[i]<last:
            return False
        else:
            last = L[i]
    return True

def est_triee2(L):
    """Determine if a list is sorted
    Keyword argument:
    L -- List of numbers
    return true or false"
    """
    last = L[0]
    i = 1
    while i < len(L):
        if L[i]<last:
            return False
        else:
            last = L[i]
            i=i+1
    return True
    
def position_tri(L,e):
    """Search an element in the list
    Keyword argument:
    L -- List of numbers
    e -- element searched
    return position of the element if found"
    """
    l = len(L)
    ind = l//2
    last = None
    while e != L[ind]:
        if last == ind:
            return -1
        if e < L[ind]:
            last = ind
            ind = ind//2
        else:
            last = ind
            ind = ind + ind//2
    return ind
        
def a_repetitions(L):
    """Determine if the list contain repetition
    Keyword argument:
    L -- List of numbers
    return true or false"
    """
    t = []
    i = 0
    while i < len(L):
        if L[i] not in t:
            t.append(L[i])
        else:
            return True
    return False


def separer(L):
    """Separe the list in two list
    Keyword argument:
    L -- List of numbers
    e -- element searched
    return void"
    """
    curr = 0
    LSEP = []
    for e in L:
        if e<0:
           LSEP.insert(0,e)
           curr +=1
        elif e==0:
            LSEP.insert(curr+1,e)
        else:
            LSEP.append(e)


#Ex4
#1
def histo(F:[int])->[int]:
    """Determine if the list contain repetition, where, and how many
    Keyword argument:
    L -- List of numbers
    e -- element searched
    return a list"
    """
    H = []
    maxValeur = val_max(F)
    for i in range(maxValeur+1):
        H.append(0)
    for i in range(len(F)):
        H[F[i]] += 1
    return H

f = [6,5,6,8,4,2,1,5]
print(histo(f))


def estInjective(F:[int])->bool:
    """Determine if the list is injective
    Keyword argument:
    F -- List of integer
    return true or false"
    """
    h = histo(F)
    for i in range(len(h)):
        if (h[i] > 1):
            return False
    return True

def estSurjective(F:[int])->bool:
    """Determine if the list is surjective
    Keyword argument:
    F -- List of integer
    return true or false"
    """
    h = histo(F)
    for i in range(len(h)):
        if (h[i] == 0):
            return False
    return True

def estBijective(F:[int])->bool:
    """Determine if the list is bijective
    Keyword argument:
    F -- List of integer
    return true or false"
    """
    if (estInjective(F) and estSurjective(F)):
        return True
    return False

#2
def afficheHisto(F:[int]):
    """Dispay the list as an histogramme
    Keyword argument:
    F -- List of integer
    return void"
    """
    h = histo(F)
    lenH = len(h)
    maxH = val_max(h)
    res = "TEST HISTOGRAMME\n\nF="+str(F)+"\n\nHISTOGRAMME\n"
    
    nbEspaces = len(str(lenH))
    
    ligne = " "
    ligneBase = "|"
    for i in range(lenH-1):
        for i in range(nbEspaces):
            ligne += " "
            ligneBase += "-"
        ligne += "|"
        ligneBase +="|"
    ligne += "\n"
    res += ligne
    
    ligneBase += "\n"
    for i in range(maxH):
        
        ligneTemp = list(ligne)
        for j in range(lenH):
            if h[j] >= maxH-i:
                print (type(ligneTemp))
                ligneTemp[(j*3)-1] = '#'
        ligneTemp = "".join(ligneTemp)
        res += ligneTemp
    res += ligneBase
    chiffres = "  1"
    
    espace = ""
    for j in range(nbEspaces):
        espace += " "
    for i in range(2,lenH):
        nb = len(str(i)) - 1
        print("espace :", espace[nb:]+"fin")
        chiffres += espace[nb:] + str(i)
    res += chiffres
    print(res)
    
        
f = [1,5,5,5,9,11,11,15,15,15]        
afficheHisto(f)
    
def afficheBelHisto(F):
    plt.hist(F)
afficheBelHisto(f)

def test_present(present:callable)->bool:
    """Test different function types
    Keyword argument:
    present -- callback
    return void"
    """
    l = [0,1,2,3,4,5,6,7,8,9,10]
    if (not present([],0)):
        print("SUCCES: test liste vide")
    else:
        print("ECHEC:  test  liste  vide")
        
    if (present(l,0)):
        print("SUCCES: test debut")
    else:
        print("ECHEC:  test debut")
        
    if (present(l,10)):
        print("SUCCES: test milieu")
    else:
        print("ECHEC:  test milieu")
        
    if (present(l,5)):
        print("SUCCES: test fin")
    else:
        print("ECHEC:  test fin")
        
    if (not present(l,200)):
        print("SUCCES: test absent")
    else:
        print("ECHEC:  test  absent")
        
        
#VERSION1
def present1(L, e) :
    for i in range (0, len(L), 1) : 
        if (L[i] == e) : 
            return(True)#stocker dans une variable
        else :            #retirer
            return (False)#retirer
    return (False) 

#VERSION2
def present2(L, e) :
    b=True#b = False
    for i in range (0, len(L), 1) : 
        if (L[i] == e) : 
            b=True
        else : #retirer
            b=False#retirer
    return (b)
            
#VERSION3
def present3(L, e) :
    b=True#b=False
    for i in range (0, len(L), 1) :
        return (L[i] == e) 
    #return b

#VERSION4
def present4(L, e) :
    b=False
    i=0
    while (i<len(L) and b) :
        if (L[i] == e) :
            b=True
        #i += 1
    return (b)
    
#test_present(present4)




def test_pos(pos:callable)->bool:
    """Test different type of function
    Keyword argument:
    pos -- callback
    return void"
    """
    l = [0,1,2,7,4,5,6,7,8,9,10]
    l1 = [7,1,2,7,4,5,6,7,8,9,10]
    l2 = [0,1,2,7,4,5,6,7,8,9,7]
    l3 = [0,1,2,3,4,5,6,3,8,9,3]
    
    if (not pos([],0)):
        print("SUCCES: test liste vide")
    else:
        print("ECHEC:  test  liste  vide")
        
    if (pos(l1,7) == [0,3,7]):
        print("SUCCES: test debut")
    else:
        print("ECHEC:  test debut")
        
    if (pos(l,7) == [3,7]):
        print("SUCCES: test milieu")
    else:
        print("ECHEC:  test milieu")
        
    if (pos(l2,7)== [3,7,10]):
        print("SUCCES: test fin")
    else:
        print("ECHEC:  test fin")
        
    if (not pos(l3,7)):
        print("SUCCES: test absent")
    else:
        print("ECHEC:  test  absent")

   

#VERSION 1
def pos1(L, e) :
    Lres= [list(L)] #Lres = []
    for i in range (0, len(L), 1):
        if (L[i] == e) :
            Lres+= [i]
    return Lres
        
        

# VERSION 2
def pos2(L, e) :
    Lres = list(L) #Lres = []
    for i in range (0, len(L), 1) :
        if (L[i] == e) :
            Lres[i]= i
    return Lres
        
        
# VERSION 3
def pos3(L, e) :
    nb=L.count(e)
    Lres= [0] * nb
    #j=0
    for i in range (0, len(L), 1) :
        if (L[i] == e) :
            Lres.append(i) #Lres[j] == i
            #j+=1
    return Lres
        
        
#VERSION4
def pos4(L, e) :
    nb=L.count(e)
    Lres= [0]*nb
    j=0
    for i in range (0, len(L), 1) :
        if (L[i] == e) :
            Lres[j]= i
            #j+=1
    return Lres



test_pos(pos3)
